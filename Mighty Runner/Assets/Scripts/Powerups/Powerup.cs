﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PowerUpType
{
    IMMUNE,
    BONUSSCORE,
    PATHLIGHT
}
public class Powerup : MonoBehaviour
{
    [Header("---Power Up---")]
    [Tooltip("The type of power up")]
    public PowerUpType type;
    [Tooltip("How long the power up lasts for")]
    public float duration = 0;
    [Tooltip("The UI displaying the powerup in use")]
    public Sprite powerUpIcon;
    //The system responsible for player movement
    private PlayerMovement player;
    //The power up sprite renderer
    private SpriteRenderer ren;
    //the current timer
    private float timer = 0;

    [HideInInspector]
    public bool activated = false; //checking if the powerup is in use

    //used to check if the current powerup is already picked up
    private bool allowPickup = true;
    [Header("---Audio---")]
    public AudioSource audioSource;


    public void Start()
    {
        //Fetch References
        player = FindObjectOfType<PlayerMovement>();
        ren = GetComponentInChildren<SpriteRenderer>();
    }


    private void Update()
    {
        if (activated) //if in use
        {
            //if the timer isnt zero and the powerup in use is this one
            if (timer >= 0 && player.powerup == this)
            {
                timer -= Time.deltaTime % 60; //continue count down
                player.powerUpTimerDisplay.text = (int)timer + "s";//update the timer UI
            }
            else
            {
                //set this one inactive
                activated = false;
                if (player.powerup == this) //if the powerup is still this one (i.e. another one hasnt been picked up)
                {
                    player.powerup = null; //set the reference null
                    //disable UI
                    player.powerUpTimerDisplay.gameObject.SetActive(false);
                    player.powerUpIconDisplay.gameObject.SetActive(false);
                }
                //detach from player
                transform.parent = null;

            }
        }
    }
    public void OnDisable()
    {
        //turn the sprite back on
        ren.gameObject.SetActive(true);
        //alow the powerup to be picked up.
        allowPickup = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player is colliding with the power up
        if (collision.tag == "Player" && allowPickup)
        {
            //set active
            activated = true;
            //set reference
            player.powerup = this;
            //disable the sprite
            ren.gameObject.SetActive(false);
            //set the timer
            timer = duration;
            //enable the UI
            player.powerUpTimerDisplay.gameObject.SetActive(true);
            player.powerUpIconDisplay.gameObject.SetActive(true);
            player.powerUpIconDisplay.sprite = powerUpIcon;
            //attach to the player to keep the object pool from despawning it when going off
            //screen
            transform.parent = player.transform;
            //dont allow pickup
            allowPickup = false;
            //play sound fx
            audioSource.Play();

        }
    }







}
