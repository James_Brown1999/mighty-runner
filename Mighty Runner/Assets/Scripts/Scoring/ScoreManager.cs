﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [Header("---Player---")]
    [Tooltip("The system responsible for player movement")]
    public PlayerMovement player;
    [Header("---Scoring---")]
    public int scoreEarnAmount = 0;
    public int powerUpMultiplier = 0;
    [HideInInspector]
    public int score = 0;
    [HideInInspector]
    public int distance = 0;
    [HideInInspector]
    public Vector2 startPos;
    [Header("---UI---")]
    [Tooltip("The UI displaying the the current score")]
    public TextMeshProUGUI scoreText;
    [Tooltip("The UI displaying the the current distance traveled")]
    public TextMeshProUGUI distanceText;


    public void Start()
    {
        //get the players origin
        startPos = transform.position;
    }

    public void Update()
    {
        if (distanceText != null) //Error check
        {
            //calculate the distance from the origin along the x axis
            distance = (int)(transform.position.x - startPos.x);
            //diplay the abs value. (Cant have negative distance)
            distanceText.text = "Distance: " + Mathf.Abs(distance) + "m";
        }
    }


    public void AddScore()
    {
        int multiplier = 1; //used for powerups, will remain one if no power up is in use

        if (player.powerup != null)
        {
            if (player.powerup.type == PowerUpType.BONUSSCORE) //bonus score powerup is active
            {
                multiplier = powerUpMultiplier; //begin use of powerup multiplier
            }
        }

        score += scoreEarnAmount * multiplier; //add to score
        scoreText.text = "Score: " + score; //update UI
    }



    



}
