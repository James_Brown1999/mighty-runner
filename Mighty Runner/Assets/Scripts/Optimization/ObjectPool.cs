﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct ObjectInfo
{
    [Tooltip("The gameobject in the pool")]
    public GameObject obj;
    [Tooltip("How many copies in the pool")]
    public int amount;
    [Tooltip("Any additional data such as positional offsets, etc")]
    public ObjectProperties properties;
    [Tooltip("How far off the left side of screen to despawn and re-enter to pool")]
    public float outOfBoundsOffset;
}

public class ObjectPool : MonoBehaviour
{
    [Tooltip("The desired objects to be added to the pool on startup")]
    public List<ObjectInfo> availableObjects = new List<ObjectInfo>();
    [HideInInspector]
    public List<ObjectInfo> unusedObjects = new List<ObjectInfo>(); //what objects are still available to be spawned
    [HideInInspector]
    public List<ObjectInfo> activeObjects = new List<ObjectInfo>(); //what objects are currently in play
    [HideInInspector]
    public ObjectInfo lastSpawned; //the last spawned pool object



    public void Update()
    {
        if (OutOfView()) //if the object is out of view (left side of the screen)
        {
            Despawn(); //despawn it and make it available again for use.
        }
    }


    /// <summary>
    /// Creates and adds the available objects to the unused pool. Recommended to be called on startup.
    /// </summary>
    /// <param name="objectData"></param>
    public void CreatePool(List<ObjectInfo> objectData)
    {
        foreach (var objInfo in objectData)
        {
            for (int i = 0; i < objInfo.amount; i++)
            {
                CreateObject(objInfo);
            }
        }
    }


    /// <summary>
    /// Same as CreatePool, except a specified object can be added into the pool instead of the whole list
    /// </summary>
    /// <param name="objectData"></param>
    public void Add(ObjectInfo objectData)
    {
        if (!activeObjects.Contains(objectData))
        {
            for (int i = 0; i < objectData.amount; i++)
            {
                CreateObject(objectData);
            }
        }
    }

    /// <summary>
    /// Create a copy of the desired object and add into the unused list.
    /// </summary>
    /// <param name="desiredObj"></param>
    public void CreateObject(ObjectInfo desiredObj)
    {
        ObjectInfo newObj = desiredObj;       
        newObj.obj = Instantiate(desiredObj.obj, desiredObj.obj.transform.position, Quaternion.Euler(0, 0, 0));
        unusedObjects.Add(newObj);
    }

    /// <summary>
    /// Checking if the last spawned object is in view of the camera.
    /// </summary>
    /// <returns></returns>
    public bool inView()
    {
        if (lastSpawned.obj != null)
        {
            Vector2 screenPoint = Camera.main.WorldToScreenPoint(lastSpawned.obj.transform.position);
            if (screenPoint.x < Screen.width)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Checking if the first active object is out of the screen.
    /// </summary>
    /// <returns></returns>
    public bool OutOfView()
    {
        if (activeObjects.Count > 0)
        {
            Vector2 screenPoint = Camera.main.WorldToScreenPoint(activeObjects[0].obj.transform.position);
            if (screenPoint.x + activeObjects[0].obj.transform.localScale.x + activeObjects[0].outOfBoundsOffset < 0)
            {
                return true;
            }
        }
       
        return false;
    }

    /// <summary>
    /// Remove the first active object out of the active pool, set it inactive and re add it to the unused pool.
    /// </summary>
    public void Despawn()
    {
        activeObjects[0].obj.SetActive(false);
        unusedObjects.Add(activeObjects[0]);
        activeObjects.RemoveAt(0);
    }
    /// <summary>
    /// Remove all active objects out of the active pool, set them inactive and re add them to the unused pool.
    /// </summary>
    public void DespawnAll()
    {
        for (int i = 0; i < activeObjects.Count;i++)
        {
            activeObjects[0].obj.SetActive(false);
            unusedObjects.Add(activeObjects[0]);
            activeObjects.RemoveAt(0);

        }
    }
    /// <summary>
    /// Remove a desired active object out of the active pool, set it inactive and re add it to the unused pool.
    /// </summary>
    /// <param name="activeObject"></param>
    public void Despawn(GameObject activeObject)
    {
        foreach(var objInfo in activeObjects)
        {
            if (objInfo.obj == activeObject)
            {
                objInfo.obj.SetActive(false);
                activeObjects.Remove(objInfo);
                unusedObjects.Add(objInfo);
                break;
            }
        }
    }

    /// <summary>
    /// Rearrange the active list.
    /// </summary>
    public void ShuffleUnusedPool()
    {
        int size = unusedObjects.Count;
        int last = size - 1;
        for (int i = 0; i < last; i++)
        {
            int r = Random.Range(i, size);
            ObjectInfo temp = unusedObjects[i];
            unusedObjects[i] = unusedObjects[r];
            unusedObjects[r] = temp;
        }
    }


}
