﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGeneration : MonoBehaviour
{
    [Tooltip("The Object Pool responsible for the obstacle objects")]
    public ObjectPool pool;


    // Start is called before the first frame update
    void Start()
    {
        //create the pool.
        pool.CreatePool(pool.availableObjects);
    }

    /// <summary>
    /// Spawn a random obstacle from the Object Pool at a desired point.
    /// </summary>
    /// <param name="point"></param>
    public void SpawnObstacle(Transform point)
    {
        if (pool.unusedObjects.Count > 0)
        {
            //shuffle the rotation.
            pool.ShuffleUnusedPool();
            //spawn it.
            pool.lastSpawned = pool.unusedObjects[0];
            pool.lastSpawned.obj.SetActive(true);
            pool.lastSpawned.obj.transform.position = point.position;
            pool.unusedObjects.RemoveAt(0);
            pool.activeObjects.Add(pool.lastSpawned);
        }
    }
}
