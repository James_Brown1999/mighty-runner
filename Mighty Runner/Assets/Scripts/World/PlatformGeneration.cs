﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlatformType
{
    STANDARD,
    LAVA
}


public class PlatformGeneration : MonoBehaviour
{
    [Tooltip("The type of platform. Standards spawn over standard backgrounds. Lava spawn over lava")]
    public PlatformType platformType;
    [Tooltip("Which child to start from from when looking for paths")]
    public int startIndex = 0;
    [Header("---Object Pools---")]
    [Tooltip("The Object Pool responsible for the platform objects")]
    public ObjectPool pool;
    [Tooltip("The Object Pool responsible for the background objects")]
    public ObjectPool backgroundPool;
    [Header("---Core System---")]
    [Tooltip("The system responsible for spawning obstacles")]
    public ObstacleGeneration obstacleGeneration;
    [Tooltip("The system responsible for spawning collectibles")]
    public CollectibleManager collectibleManager;
    

    public void Start()
    {
        if (platformType == PlatformType.STANDARD) //create object pool if standard type
        {
            pool.CreatePool(pool.availableObjects);
        }
    }


    public void OnDisable()
    {
        //if lava set all childs from the index offset inactive. (All children from the startIndex are premade paths.)
        if (platformType == PlatformType.LAVA) 
        {
            for (int i = startIndex; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Spawn a platform and apply a random y offset.
    /// </summary>
    /// <param name="point"></param>
    public void SpawnPlatform()
    {
        if (pool.unusedObjects.Count > 0) //still platforms left to spawn.
        {
            //set the lastspawn and spawn the item from the object pool.
            pool.lastSpawned = pool.unusedObjects[0];
            pool.unusedObjects.RemoveAt(0);
            pool.activeObjects.Add(pool.lastSpawned);
            pool.lastSpawned.obj.SetActive(true);

            //calculate a random y offset
            Random.InitState((int)System.DateTime.Now.Ticks);
            float yOffset = Random.Range(0.59f, 2.96f);

            //set its position to be the random y offset and last backgrounds x position.
            pool.lastSpawned.obj.transform.position = new Vector3(backgroundPool.lastSpawned.obj.transform.position.x, yOffset, -12.34766f);

            //get the spawn point child transform.
            Transform miscSpawnPoint = pool.lastSpawned.obj.transform.GetChild(pool.lastSpawned.obj.transform.childCount - 1);
            //if value is less 0.4f, spawn an obstacle on the platform.
            if (Random.value <= 0.4f)
            {
                obstacleGeneration.SpawnObstacle(miscSpawnPoint);
            }
            //if value is greater than 0.6f, spawn a collectible on the platform.
            else if (Random.value >= 0.6f)
            {
                collectibleManager.SpawnCollectible(miscSpawnPoint);
            }
        }
    }

    /// <summary>
    /// Choose a random premade path for navigating over the lava.
    /// </summary>
    public void ChooseLavaPath()
    {
          int ran = Random.Range(startIndex, transform.childCount);
          transform.GetChild(ran).gameObject.SetActive(true);
    }
}
