﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gems : MonoBehaviour
{
    //The gems sprite
    private SpriteRenderer ren;
    //The system responsible for spawning collectibles
    [HideInInspector]
    public CollectibleManager collectibleManager;

    private void OnEnable()
    {
        //randomly pick a colour.
        ren = GetComponent<SpriteRenderer>();
        Random.InitState((int)System.DateTime.Now.Ticks);
        float r = Random.Range(100, 256);
        float g = Random.Range(100, 256);
        float b = Random.Range(100, 256);

        ren.color = new Color(r / 255, g / 255, b / 255, 1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if collision is with the player
        if (collision.tag == "Player")
        {
            //get the death system
            PlayerDeath player = collision.GetComponent<PlayerDeath>();
            //play the pickup sound
            player.audioSource.clip = player.gemPickUp;
            player.audioSource.Play();
            //Add to score
            collectibleManager.scoreManager.AddScore();
            collectibleManager.addScore = true;
            //despawn the gameobject.
            collectibleManager.pool.Despawn(gameObject);
        }
    }
}
