﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="ObjectInfo",menuName ="ObjectPool",order = 0)]
public class ObjectProperties : ScriptableObject
{
    [Header("---Offsets---")]
    [Tooltip("How much do you wish to offset the objects spawn position from the last spawned.")]
    public float spawnOffset = 0;
    [Tooltip("How much do you wish to scale the offset. Leave at 1 if you dont desire scaling.")]
    public float nexObjOffsetScalar = 0;
}
