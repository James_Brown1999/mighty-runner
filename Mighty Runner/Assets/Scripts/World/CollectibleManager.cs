﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleManager : MonoBehaviour
{
    [Header("---Object Pools---")]
    [Header("The object pool responsible for this system")]
    public ObjectPool pool;
    [Header("The Obejct pool responsible for the backgrounds")]
    public ObjectPool backgrounds;
    [Header("---Scoring---")]
    [Tooltip("The system responsible for adding score and calculating distance traveled")]
    public ScoreManager scoreManager;
    [HideInInspector]
    public bool addScore = false;

    private void Start()
    {
        //create the pool
        pool.CreatePool(pool.availableObjects);
    }

    private void Update()
    {
        //if add score
        if (addScore)
        {
            scoreManager.AddScore();
            addScore = false;
        }
    }

    /// <summary>
    /// Spawning the gems out of the object pool at a desired point.
    /// </summary>
    /// <param name="point"></param>
    public void SpawnCollectible(Transform point)
    {
        if (pool.unusedObjects.Count > 0) //if there are gems still left to be placed in the pool.
        {
            pool.lastSpawned = pool.unusedObjects[0]; //set the last spawned
            //collectible manager hasnt been assigned to the gem
            if (pool.lastSpawned.obj.GetComponent<Gems>().collectibleManager == null) 
            {
                //assign it
                pool.lastSpawned.obj.GetComponent<Gems>().collectibleManager = this;
            }
            //set the gems position to the point
            pool.lastSpawned.obj.transform.position = point.position;
            //set it active
            pool.lastSpawned.obj.SetActive(true);
            //remove from unused pool
            pool.unusedObjects.RemoveAt(0);
            //add it the active pool.
            pool.activeObjects.Add(pool.lastSpawned);
        }
    }


}
