﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [Header("---Object Pools---")]
    [Tooltip("The Object Pool responsible for the background objects")]
    public ObjectPool pool;
    [Header("---Object Info---")]
    [Tooltip("The lava backgrounds to be unlocked during gameplay")]
    public List<ObjectInfo> lavaBackgrounds = new List<ObjectInfo>();
    [Tooltip("The distance to unlock the first lava variant background")]
    public float distToUnlockLavaVar1 = 0;
    [Tooltip("The distance to unlock the second lava variant background")]
    public float distToUnlockLavaVar2 = 0;
    [Tooltip("The distance to unlock the third lava variant background")]
    public float distToUnlockLavaVar3 = 0;
    private bool lava1Unlocked = false;
    private bool lava2Unlocked = false;
    private bool lava3Unlocked = false;
    [Header("---Core System---")]
    [Tooltip("The system responsible for spawning collectibles")]
    public CollectibleManager collectibleManager;
    [Tooltip("The system responsible for spawning platforms")]
    public PlatformGeneration standardPlatformGeneration;
    [Tooltip("The system responsible for spawning obstacles")]
    public ObstacleGeneration obstacleGeneration;
    [Tooltip("The system responsible for adding score and calculating distance traveled")]
    public ScoreManager scoreManager;
    private bool dontSpawnCollectibles = true;

    private bool startSectionGenerated = false;



    // Start is called before the first frame update
    void Start()
    {
        //create pool
        pool.CreatePool(pool.availableObjects);
        //spawn background
        SpawnBackground();
        //allow collectibles to be spawned.
        dontSpawnCollectibles = false;
        //start section has been generated.
        startSectionGenerated = true;
    }

    private void Update()
    {
        if (pool.inView()) //the last spawned object is in view, spawn next series of backgrounds
        {
            SpawnBackground();
        }
        //if the min distance for lava1 is reached. Add the lava varient 1 to the pool and spawn rotation.
        if (scoreManager.distance >= distToUnlockLavaVar1 && !lava1Unlocked)
        {     
            pool.Add(lavaBackgrounds[0]);
            lava1Unlocked = true;
        }
        //if the min distance for lava2 is reached. Add the lava varient 2 to the pool and spawn rotation.
        if (scoreManager.distance >= distToUnlockLavaVar2 && !lava2Unlocked)
        {
            pool.Add(lavaBackgrounds[1]);
            lava2Unlocked = true;
        }
        //if the min distance for lava3 is reached. Add the lava varient 3 to the pool and spawn rotation.
        if (scoreManager.distance >= distToUnlockLavaVar3 && !lava3Unlocked)
        {
            pool.Add(lavaBackgrounds[2]);
            lava3Unlocked = true;
        }
    }

    /// <summary>
    /// Spawning the background via with the object pool.
    /// </summary>
    public void SpawnBackground()
    {
        for (int i = 0; i < pool.unusedObjects.Count; i++)
        {
            //shuffle the pool to add randomness to the background being selected
            pool.ShuffleUnusedPool();

            //get the spawnPos
            Vector2 spawnPos = pool.unusedObjects[0].obj.transform.position;
            if (pool.lastSpawned.obj != null)//there is a last spawned object
            {
                //calculate the offset
                float offsetX = pool.lastSpawned.properties.spawnOffset * pool.lastSpawned.properties.nexObjOffsetScalar; 
                spawnPos = (Vector2)pool.lastSpawned.obj.transform.position + new Vector2(offsetX, 0);

            }
            //set the last spawned
            pool.lastSpawned = pool.unusedObjects[0];
            //set the position
            pool.lastSpawned.obj.transform.position = spawnPos;
            //set the rotation
            pool.lastSpawned.obj.transform.rotation = Quaternion.Euler(0, 0, 0);
            //set it active
            pool.lastSpawned.obj.SetActive(true);
            //remove it from the unused pool
            pool.unusedObjects.RemoveAt(0);
            //add it to the active pool
            pool.activeObjects.Add(pool.lastSpawned);
            //if the background being spawned is a lava type
            if (pool.lastSpawned.obj.name.Contains("Lava"))
            {
                //select a path to spawn above the lava
                pool.lastSpawned.obj.GetComponent<PlatformGeneration>().ChooseLavaPath();
            }
            else
            {
                if (startSectionGenerated) //Start section has been generated
                {
                    standardPlatformGeneration.SpawnPlatform(); //spawn a platform

                    //all children are spawn points for obstacles and coins
                    for (int j = 0; j < pool.lastSpawned.obj.transform.childCount; j++)
                    {
                        //pick a random value
                        float ran = Random.value;
                        //if its greater than 0.6f, spawn a collectible
                        if (ran >= 0.6f)
                        {
                            collectibleManager.SpawnCollectible(pool.lastSpawned.obj.transform.GetChild(j));
                        }
                        else if (ran <= 0.4f) //if its less than 0.4f, spawn a obstacle
                        {
                            obstacleGeneration.SpawnObstacle(pool.lastSpawned.obj.transform.GetChild(j));
                        }
                        else //dont spawn anything.
                        {
                            continue;
                        }
                    }
                }
            }
        }
    }


}
