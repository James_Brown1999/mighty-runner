﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Tooltip("The system responsible for player movement")]
    public PlayerMovement playerMovement;
    [Header("---Camera Movement---")]
    [HideInInspector]
    public float velocity = 0;
    [Tooltip("how fast the camera speeds up")]
    public float acceleration = 0;
    [Tooltip("The max speed the camera can travel at")]
    public float normalSpeed = 0;
    [HideInInspector]
    public Vector2 startPos; //the cameras start pos

    public void Start()
    {
        //get origin
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //add to velocity
        velocity += acceleration * Time.deltaTime;

        //clamp it
        velocity = Mathf.Clamp(velocity, 0, normalSpeed);

        //Error checking
        if (playerMovement != null && Time.timeScale == 1)
        {
            //translate it.
            transform.Translate(Vector3.right * velocity, Space.World);
        }
    }
    /// <summary>
    /// Shake the camera for a specific amount of time and intensity
    /// </summary>
    /// <param name="duration"></param>
    /// <param name="magnitude"></param>
    /// <returns></returns>
    public IEnumerator Shake (float duration, float magnitude)
    {
        //get the original pos
        Vector3 originalPos = Camera.main.transform.localPosition;

        float elapsed = 0;

        while (elapsed < duration)
        {
            //calculate random offsets
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            //apply it
            Camera.main.transform.localPosition = new Vector3(x, y, originalPos.z);

            //tick down
            elapsed += Time.deltaTime;

            yield return null;
        }

        //reset the position.
        Camera.main.transform.localPosition = originalPos;
    }
}
