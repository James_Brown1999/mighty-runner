﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//THIS CLASS IS NO LONGER USED.
public class PlayerSpawn : MonoBehaviour
{
    public ObjectPool platformPool;
    private bool spawned = false;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!spawned)
        {
            transform.position = (Vector2)platformPool.activeObjects[0].obj.transform.position - new Vector2(5.5f, 0);
            spawned = true;
        }
    }
}
