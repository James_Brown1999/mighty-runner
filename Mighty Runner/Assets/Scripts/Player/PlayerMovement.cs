﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Rewired;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D m_rigidBody; //players rigidbody
    private Player input; //rewired input
    public Animator animator;
    public Transform boxCast;
    [Tooltip("How fast is the player accelerating")]
    public float acceleration = 0;
    [Tooltip("The normal speed the player is moving.")]
    public float maxSpeed = 0;
    [Tooltip("How high do you want the player to jump")]
    public float jumpForce = 0;
    public float jumpDelay = 0;
    private float delayTimer = 0;
    private bool delayJump = false;
    private bool grounded = false;
    [HideInInspector]
    public float velocity = 0;
    [Header("---Power Ups---")]
    public Powerup powerup;
    public Image powerUpIconDisplay;
    public TextMeshProUGUI powerUpTimerDisplay;
    [Header("---Audio---")]
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        //fetch references
        m_rigidBody = GetComponent<Rigidbody2D>();
        input = ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.timeScale == 1)
        {
            velocity += acceleration * Time.deltaTime;
            grounded = isGrounded();

            animator.SetBool("IsGrounded", grounded);
            if (input.GetButtonDown("Jump"))
            {
                Jump();

            }


            velocity = Mathf.Clamp(velocity, 0, maxSpeed);

            //translate the player
            transform.Translate(Vector2.right * velocity, Space.World);
        }
   
    }


    public bool InView()
    {
        //Get the screen position of the player.
        Vector2 playerScreenPos = Camera.main.WorldToScreenPoint(transform.position);
        //check if its within the screen.
        return playerScreenPos.x > (0.138f * 10) && playerScreenPos.x < Screen.width - (0.138f * 10);
    }


    public void Jump()
    {
        //check if the player is grounded
        if (grounded && !delayJump && Time.timeScale == 1)
        {
            audioSource.Play();
            //it is, apply force to rigidbody
            delayJump = true;
            m_rigidBody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

    }

    private bool isGrounded()
    {

        if (delayJump)
        {
            delayTimer += Time.deltaTime % 60;
            if (delayTimer >= jumpDelay)
            {
                delayJump = false;
                delayTimer = 0;
            }
        }
        //return true if we get a hit, using boxcast to detect if the player is on a platform edges.
        return Physics2D.BoxCast(boxCast.transform.position, boxCast.transform.localScale, 0, -Vector2.up, 0.001f); 
    }

}
