﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    [Tooltip("The system responsible for player movement")]
    public PlayerMovement playerMovement;
    [Tooltip("The system responsible for camera movement")]
    public CameraController cameraController;
    [Tooltip("The system responsible for game flow")]
    public GameManager gameManager;
    [Tooltip("The explosion animation")]
    public Animator explosion;
    [Header("---Audio---")]
    [Tooltip("Colliding with an obstacle sound")]
    public AudioClip obstacleCollision;
    [Tooltip("player death sound")]
    public AudioClip deathSound;
    [Tooltip("Colliding with a gem sound")]
    public AudioClip gemPickUp;
    public AudioSource audioSource;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        //if colliding with lava or spikes
        if (collision.tag == "Lava" || collision.tag == "Spikes")
        {
            //if the player has a power up
            if (playerMovement.powerup != null)
            {
                //if the player has the immunity power up and not colliding with lava
                if (playerMovement.powerup.type == PowerUpType.IMMUNE && collision.tag != "Lava")
                {
                    //exit.
                    return;
                }
            }

            //play explosion animation and end game.
            explosion.gameObject.SetActive(true);
            gameManager.EndGame();
            audioSource.clip = deathSound;
            audioSource.Play();
        }
        else
        {
            //collding with an obstacle
            if (collision.GetComponent<Particles>() != null)
            {
                //play the destruction particles and shake camera.
                collision.transform.GetChild(0).gameObject.SetActive(false);
                collision.GetComponent<Particles>().PlayDestructionPartcles();
                StartCoroutine(cameraController.Shake(0.5f, 0.1f));
                audioSource.clip = obstacleCollision;
                audioSource.Play();
            }
        }
    }

}
