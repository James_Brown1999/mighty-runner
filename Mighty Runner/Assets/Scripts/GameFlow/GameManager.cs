﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;
using UnityEngine.SceneManagement;
using TMPro;
using Rewired;


public class GameManager : MonoBehaviour
{
    [Header("---Core System---")]
    [Tooltip("The system responsible for player movement")]
    public PlayerMovement playerMovement;
    [Tooltip("The players rigidbody")]
    public Rigidbody2D playerRigidBody;
    [Tooltip("The system responsible for camera movement")]
    public CameraController cameraController;
    [Tooltip("The system responsible for adding score and calculating distance")]
    public ScoreManager scoreManager;
    [Tooltip("The system responsible for spawing the background")]
    public Background background;
    [Header("---UI----")]
    [Tooltip("The main menu UI object")]
    public GameObject mainMenuUI;
    [Tooltip("The gameplay UI object")]
    public GameObject gamePlayUI;
    [Tooltip("The game over UI object")]
    public GameObject gameOverUI;
    [Tooltip("The mute button UI object")]
    public GameObject muteButton;
    [Tooltip("The unmute button UI object")]
    public GameObject unMuteButton;
    [Tooltip("The pause button UI object")]
    public GameObject pauseButton;
    [Tooltip("The play button UI object")]
    public GameObject playButton;
    [Tooltip("The final score UI text")]
    public TextMeshProUGUI finalScore;
    [Tooltip("The final distance UI text")]
    public TextMeshProUGUI totalDistance;
    [Tooltip("The worlds light source")]
    public Light2D globalLight;
    [Tooltip("The start game UI text")]
    public TextMeshProUGUI startText;
    [Tooltip("The end game UI text")]
    public TextMeshProUGUI endGameText;
    [Header("---Audio---")]
    public AudioSource audioSource;
    [Tooltip("The sound to play when the player dies.")]
    public AudioClip deathSound;

    [Header("---Controls----")]
    [Tooltip("The android controls UI object")]
    public GameObject androidControl;
    [Tooltip("The pc controls UI object")]
    public GameObject pcControl;
    [Tooltip("The xbox controls UI object")]
    public GameObject xboxControl;
    [Tooltip("The ps4 controls UI object")]
    public GameObject ps4Control;
    [Tooltip("The end game Rewired touch region")]
    public GameObject endGameUITouch;
    [Tooltip("The main menu Rewired touch region")]
    public GameObject mainMenuTouch;
    [Tooltip("The gameplay Rewired touch region")]
    public GameObject gameplayTouch;


    private Player input; //Rewired
    //PC controller IDs
    private string ps4ID = "Wireless Controller";
    private string xboxID = "XInput Gamepad";

    private void Start()
    {
        //fetch input
        input = ReInput.players.GetPlayer(0);
    }


    public void BeginGame()
    {
        //enable player movement
        playerMovement.enabled = true;
        //enable camera movement
        cameraController.enabled = true;
        //enable player physics
        playerRigidBody.simulated = true;
        //set the players position to the start position
        playerMovement.transform.position = scoreManager.startPos;
        if (AudioListener.pause) //if the audio is muted
        {
            //set correct UI
            muteButton.SetActive(false);
            unMuteButton.SetActive(true);
        }
        else
        {
            //set correct UI
            muteButton.SetActive(true);
            unMuteButton.SetActive(false);
        }
#if UNITY_STANDALONE_WIN //disable android UI if windows build
        androidControl.SetActive(false);
#endif
    }


    private void Update()
    {


#if UNITY_ANDROID || UNITY_IOS //lock the resolution on IOS and Android
        Screen.SetResolution(1920, 1080, true);
#endif
        //if the start text is enabled
        if (startText.gameObject.activeInHierarchy)
        {
#if UNITY_ANDROID || UNITY_IOS
            startText.text = "Tap to start";
#endif
            //if PC build
#if UNITY_STANDALONE_WIN	

            if (input.GetButtonDown("ToggleMenu")) //if the menu interaction button is pressed
            {
        // begin game and enable the correct touch regions (Player can use mouse to interact with the touch region)
                BeginGame();
                mainMenuUI.SetActive(false);
                gamePlayUI.SetActive(true);
                mainMenuTouch.SetActive(false);
                gameplayTouch.SetActive(true);
            }
            //if the player has a controller plugged in
            if (input.controllers.joystickCount > 0)
            {
                 //if its an PS4 controller
                if (input.controllers.Joysticks[0].hardwareName.Contains(ps4ID))
                {
                    //set correct UI
                    startText.text = "Press X to start";
                }
                 //if its an Xbox controller
                if (input.controllers.Joysticks[0].hardwareName.Contains(xboxID))
                {
                    //set correct UI
                    startText.text = "Press A to start";
                }
            }
            else //No controllers
            {
               //set correct UI
                startText.text = "Press Enter to start";
            }
#endif
        }


#if UNITY_STANDALONE_WIN //if PC
        //in game over menu
        if (gameOverUI.activeInHierarchy)
        {
            //if the player has a controller plugged in
            if (input.controllers.joystickCount > 0)
            {
                 //if its an PS4 controller
                if (input.controllers.Joysticks[0].hardwareName.Contains(ps4ID))
                {
                    //set correct UI
                    endGameText.text = "X to Return To Menu";
                }
                 //if its an Xbox controller
                if (input.controllers.Joysticks[0].hardwareName.Contains(xboxID))
                {
                    //set correct UI
                    endGameText.text = "A to Return To Menu";
                }
            }
            else //No controllers 
            {
                //set correct UI
                endGameText.text = "Enter to Return To Menu";
            }

            if (input.GetButtonDown("ToggleMenu"))
            {
                //return to the main menu
                ReturnToMenu();
            }
        }
        //if in gameplay
        if (gamePlayUI.activeInHierarchy)
        {
            //if the player has a controller plugged in
            if (input.controllers.joystickCount > 0)
            {
                //disable the PC control UI
                pcControl.SetActive(false);
                 //if its an PS4 controller
                if (input.controllers.Joysticks[0].hardwareName.Contains(ps4ID))
                {
                    //Set correct UI
                    xboxControl.gameObject.SetActive(false);
                    ps4Control.gameObject.SetActive(true);
                }
                 //if its an Xbox controller
                if (input.controllers.Joysticks[0].hardwareName.Contains(xboxID))
                {
                    //Set correct UI
                    xboxControl.gameObject.SetActive(true);
                    ps4Control.gameObject.SetActive(false);
                }
            }
            else //No controllers
            {
                //Set correct UI
                xboxControl.gameObject.SetActive(false);
                ps4Control.gameObject.SetActive(false);
                pcControl.gameObject.SetActive(true);
            }
            //if the pause button is pressed and the game isnt paused already
            if (input.GetButtonDown("Pause") && Time.timeScale == 1) 
            {
                //Pause the game
                PauseGame();
                //Set correct UI
                playButton.SetActive(true);
                pauseButton.SetActive(false);
                return;
            }
            //if the pause button is pressed and the game is paused already
            if (input.GetButtonDown("Pause") && Time.timeScale == 0)
            {
                //Unpause
                ResumeGame();
                //Set correct UI
                playButton.SetActive(false);
                pauseButton.SetActive(true);
                return;
            }
            //if the mute button is pressed and the audio isnt muted already
            if (input.GetButtonDown("Mute") && !AudioListener.pause)
            {
                //mute
                MuteAudio();
                //Set correct UI
                muteButton.SetActive(false);
                unMuteButton.SetActive(true);
                return;
            }
            //if the mute button is pressed and the audio is muted already
            if (input.GetButtonDown("Mute") && AudioListener.pause)
            {
                //unmute
                UnmuteAudio();
                //Set correct UI 
                muteButton.SetActive(true);
                unMuteButton.SetActive(false);
                return;
            }
            //if the quit button is pressed
            if (input.GetButtonDown("Quit"))
            {
                //quit to menu
                ReturnToMenu();
            }

        }
#endif
        //if the player has a power up
        if (playerMovement.powerup != null)
        {
            //if the powerup up is the light type
            if (playerMovement.powerup.type == PowerUpType.PATHLIGHT)
            {
                //increase global light source intensity
                globalLight.intensity = 2.3f;
            }
            else //power up is no longer the light type
            {
                //set light sources intensity back to original value
                if (globalLight.intensity != 0.29f)
                {
                    globalLight.intensity = 0.29f;
                }
            }
        }
        else //no longer has a power up.
        {
            //set light sources intensity back to original value
            if (globalLight.intensity != 0.29f)
            {
                globalLight.intensity = 0.29f;
            }
        }
    }

    /// <summary>
    /// reload the main scene and set timescale to 1.
    /// </summary>
    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }

    /// <summary>
    /// Set timescale to 0.
    /// </summary>
    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    /// <summary>
    /// Set timescale to 1.
    /// </summary>
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
    /// <summary>
    /// Mute the audio.
    /// </summary>
    public void MuteAudio()
    {
        AudioListener.pause = true;
    }
    /// <summary>
    /// Unmute the audio.
    /// </summary>
    public void UnmuteAudio()
    {
        AudioListener.pause = false;
    }
    /// <summary>
    /// disable the player and camera movement. Diable the gameplay UI and play the death sound.
    /// </summary>
    public void EndGame()
    {
        playerMovement.enabled = false;
        playerRigidBody.simulated = false;
        cameraController.enabled = false;
        audioSource.clip = deathSound;
        audioSource.loop = false;
        audioSource.Play();
        gamePlayUI.SetActive(false);
    }
    /// <summary>
    /// Enable the gameover UI.
    /// </summary>
    public void EndGameUI()
    {
        gameOverUI.SetActive(true);
        finalScore.text = "Final Score: " + scoreManager.score;
        totalDistance.text = "Total Distance: " + scoreManager.distance;
        endGameUITouch.SetActive(true);
    }


}
