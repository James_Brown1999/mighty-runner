﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;
using Pixelplacement;

public class AnimationEventHandler : MonoBehaviour
{
    [Header("---Managers---")]
    public GameManager gameManager;
    [Header("---Player---")]
    [Tooltip("The system responsible for player movement")]
    public PlayerMovement player;
    [Tooltip("The sprite object representing the player")]
    public GameObject playerDisplay;
    [Tooltip("The sprite object representing the player's death")]
    public GameObject angelDisplay;
    [Header("---Players death sequence---")]
    [Tooltip("How long it takes for the players angel to ascend to heaven")]
    public float ascendDuration = 0;
    [Tooltip("The light source of the beam")]
    public Light2D heavenLight;
    [Tooltip("The standard light sourcem that is attached to the player during gameplay.")]
    public Light2D standardLight;

    //Animation Event
    public void Dead()
    {
        //disable powerup
        player.powerup = null;
        //disable the regular player sprite
        playerDisplay.SetActive(false);
        //enable the angel sprite
        angelDisplay.SetActive(true);
    }

    public void AscendToHeaven()
    {
        //turn on the heaven beam
        heavenLight.gameObject.SetActive(true);
        //disable the regular light
        standardLight.gameObject.SetActive(false);
        //begin sequence
        StartCoroutine(TurnOnBeam(6));
        
    }



    IEnumerator TurnOnBeam(float duration)
    {
        //how much time has elapsed
        float elapsedTime = 0;
        //the amount to add each pass to the beam
        float intervals = 16.56f / duration;

        while (elapsedTime <= duration)
        {
            //expand the light sources outer radius
            heavenLight.pointLightOuterRadius += intervals;
            //clamp it incase it goes beyond the desired radius
            heavenLight.pointLightOuterRadius = Mathf.Clamp(heavenLight.pointLightOuterRadius, 0, 16.56f);
            //increase elapsed time.
            elapsedTime += Time.deltaTime % 60;
           
        }
        //get the offscreen position to tween to
        Vector2 offScreenPosition = new Vector2(angelDisplay.transform.position.x, angelDisplay.transform.position.y + 30);
        //tween to it
        Tween.Position(angelDisplay.transform, offScreenPosition, ascendDuration, 2,null,Tween.LoopType.None, gameManager.EndGameUI);
        //disable this object. (Attached to the smoke animation.)
        transform.gameObject.SetActive(false);
        yield return null;

    }




}
