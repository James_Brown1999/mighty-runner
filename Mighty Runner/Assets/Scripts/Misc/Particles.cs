﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particles : MonoBehaviour
{
    [Header("---Particles---")]
    [Tooltip("The particles that are desired to be played on collision")]
    public List<ParticleSystem> particles = new List<ParticleSystem>();



    public void OnDisable()
    {
        //re-enable the object sprite object
        transform.GetChild(0).gameObject.SetActive(true);
    }


    /// <summary>
    /// Loop through list of particles and Play them.
    /// </summary>
    public void PlayDestructionPartcles()
    {
        foreach (var particle in particles)
        {
            particle.Play();
        }
    }


}
