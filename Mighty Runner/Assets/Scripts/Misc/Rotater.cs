﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    [Tooltip("How fast the object rotates")]
    public float rotateSpeed = 0;

    // Update is called once per frame
    void Update()
    {
        //rotate it.
        transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime);
    }
}
